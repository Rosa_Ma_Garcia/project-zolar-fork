import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { AgmCoreModule } from '@agm/core';


import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { Conteiner1Component } from './conteiner1/conteiner1.component';
import { HomeComponent } from './home/home.component';
import { GooglemapsComponent } from './googlemaps/googlemaps.component';
import { LoginComponent } from './login/login.component';


import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { MapsComponent } from './maps/maps.component';


// import { DragulaModule } from 'ng2-dragula';




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    Conteiner1Component,
    HomeComponent,
    GooglemapsComponent,
    LoginComponent,
    MapsComponent,
 
  
  ],


  imports: [
    BrowserModule,
    NavbarComponent,
    FormsModule,
    SweetAlert2Module,
    // DragulaModule,
   
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCIdxjSv6ryGgndpGEofce3YfLSTJV-W7g',
      // libraries:["places"],
    }),

    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
   
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
  })
   
  ],



  providers: [],
  bootstrap: [AppComponent],

 
})


export class AppModule { }
