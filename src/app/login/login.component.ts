import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent {
  constructor(public afAuth: AngularFireAuth) {
  }

  //Login con Google
  login() {
      this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider()).then(function(result){
        // console.log(result.user);
        var data = result.user;
        console.log(data);
        var name = data.displayName;
         console.log(name);
         var email = data.email;
         console.log(email);
         
      });
     
  }
  logout(){
    return this.afAuth.auth.signOut();
  }




  //Login con Facebook
  loginFace(){
    this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider()).then(function(res){
      console.log(res);
      
    });
  }

  
}



