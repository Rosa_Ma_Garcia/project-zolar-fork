import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-googlemaps',
  templateUrl: './googlemaps.component.html',
  styleUrls: ['./googlemaps.component.css']
})
export class GooglemapsComponent {
  title: string = 'Google Maps con Angular';
  

  //Posicion Inicial
  lat: number = 19.4978;
  lng: number = -99.1269;
  zoom: number = 13;
  

    onChoseLocation(event){
      console.log(event);

  }

}

