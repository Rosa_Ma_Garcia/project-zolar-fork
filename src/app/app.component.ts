
import { Component, OnInit, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  imports: [BrowserModule],
  declarations:[NavbarComponent],
  bootstrap: [NavbarComponent],
  
  })

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {


  ngOnInit() {
  }
  
  }


