import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Conteiner1Component } from './conteiner1.component';

describe('Conteiner1Component', () => {
  let component: Conteiner1Component;
  let fixture: ComponentFixture<Conteiner1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Conteiner1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Conteiner1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
